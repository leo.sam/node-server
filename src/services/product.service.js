import ProductModel from "../models/product.model";

/**
 * @desc Creates product
 **/
export const createProduct = async ({ title, price, description, status }) => {
  const product = await ProductModel.create({ title, price, description, status });

  return {
    product,
  };
};
