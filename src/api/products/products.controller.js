import { createProduct } from "../../services/product.service";
/**
 * @desc Повертає список товарів
 **/
export const CreateProductController = async (request, response) => {
  const { title, price, description, status } = request.body;

  const { product } = await createProduct({ title, price, description, status });

  response.status(201).json({
    status: true,
    product: product,
  });
};
